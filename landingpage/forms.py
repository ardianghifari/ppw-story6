from django import forms
from .models import Statusku

class Form_Statusku(forms.ModelForm):

	class Meta:
		model = Statusku
		fields = ['status']