from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from .forms import Form_Statusku
from .models import Statusku

# Create your views here.

def view_form(request):
	status = Statusku.objects.all().values()
	form = Form_Statusku()
	if request.method == 'POST':
		form = Form_Statusku(request.POST)
		if form.is_valid():
			status_baru = form.save()
			return HttpResponseRedirect(reverse('form'))
		else:
			form = Form_Statusku()
	return render(request, 'Form Statusku.html', {'form':form, 'status':status})