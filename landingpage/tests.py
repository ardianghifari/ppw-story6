import time
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import view_form
from .models import Statusku
from .forms import Form_Statusku


# Create your tests here.
class StatuskuTest(TestCase):
    
    def test_landingpage_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_using_form_jadwalku_template(self):
    	response = Client().get('')
    	self.assertTemplateUsed(response, 'Form Statusku.html')

    def test_statusku_database(self):
    	Statusku.objects.create(status='Test statusku')
    	self.assertEqual(Statusku.objects.all().count(), 1)

    def test_text_hello_apa_kabar(self):
    	response = Client().get('')
    	html_response = response.content.decode('utf8')
    	self.assertIn('Hello, apa kabar?', html_response)

    def test_input_status(self):
    	form = Form_Statusku()
    	response = Client().get('')
    	self.assertIn('id="id_status"', form.as_p())

    def test_can_save_a_POST_request(self):
    	response = Client().post('',
    		data={'status':'ngoding PPW'})
    	self.assertEqual(Statusku.objects.all().count(), 1)
  
    	self.assertRedirects(response, '/')

    	self.assertEqual(response.status_code, 302)
    	self.assertEqual(response['location'], '/')

    	new_response = Client().get('')
    	html_response = new_response.content.decode('utf8')
    	self.assertIn('ngoding PPW', html_response)

class StatuskuFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25) 
        super(StatuskuFunctionalTest, self).setUp()

    def test_title_text_exists(self):
        self.browser.get('http://127.0.0.1:8000/')
        title_text = self.browser.find_element_by_id('id_title').text
        self.assertIn(title_text, self.browser.page_source)

    def test_status_box_exists(self):
        self.browser.get('http://127.0.0.1:8000/')
        status_box = self.browser.find_element_by_id('id_status').get_attribute("id")
        self.assertIn(status_box, self.browser.page_source)

    def test_form_styling(self):
        self.browser.get('http://127.0.0.1:8000/')
        form_styling = self.browser.find_element_by_id('form').value_of_css_property('background-color')
        self.assertIn('rgba(153, 153, 255, 1)', form_styling)

    def test_title_text_styling(self):
        self.browser.get('http://127.0.0.1:8000/')
        title_text_styling = self.browser.find_element_by_id('id_title').value_of_css_property('font-size')
        self.assertIn('36px', title_text_styling)

    def test_can_submit_status(self):
        self.browser.get('http://127.0.0.1:8000/')
        status_box = self.browser.find_element_by_id('id_status')
        submit_box = self.browser.find_element_by_id('id_button')
        text = 'Coba coba'
        status_box.send_keys(text)
        submit_box.click()
        time.sleep(5)
        self.assertIn(text, self.browser.page_source)

    def tearDown(self):
        self.browser.implicitly_wait(5)
        self.browser.quit()