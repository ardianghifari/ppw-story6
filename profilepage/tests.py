from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import view_profile

# Create your tests here.

class ProfilkuTest(TestCase):

	def test_landingpage_url_is_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 200)

	def test_landingpage_using_ardian_ghifari_template(self):
		response = Client().get('/profile/')
		self.assertTemplateUsed(response, 'Ardian Ghifari.html')		